package com.esoft.order.api.requests;

import com.esoft.order.core.enums.OrderCategory;
import com.esoft.order.core.enums.ServiceName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class UpdateOrderRequest {

    private OrderCategory category;

    private ServiceName serviceName;

    private String description;

    private String notes;

    private Integer itemsQuantity;

    private BigDecimal totalAmount;
}
