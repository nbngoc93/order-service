package com.esoft.order.api.responses;

import com.esoft.order.core.enums.OrderCategory;
import com.esoft.order.core.enums.ServiceName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.Instant;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class OrderResponse {

    private String reference;
    private Instant createdAt;
    private Instant updatedAt;
    private String userId;
    private OrderCategory category;
    private ServiceName serviceName;
    private String description;
    private String notes;
    private Integer itemsQuantity;
    private BigDecimal totalAmount;
}
