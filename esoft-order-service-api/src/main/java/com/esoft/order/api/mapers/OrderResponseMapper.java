package com.esoft.order.api.mapers;

import com.esoft.order.api.responses.OrderResponse;
import com.esoft.order.api.responses.OrderSummaryResponse;
import com.esoft.order.core.entities.OrderEntity;
import com.esoft.order.core.entities.OrderSummaryEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class OrderResponseMapper {

    public abstract OrderResponse mapOrderEntityToOrderResponse(OrderEntity entity);
    public abstract List<OrderResponse> mapOrderEntitiesToOrderResponses(List<OrderEntity> entities);
    public abstract OrderSummaryResponse mapOrderSummaryEntityToResponse(OrderSummaryEntity entity);
}
