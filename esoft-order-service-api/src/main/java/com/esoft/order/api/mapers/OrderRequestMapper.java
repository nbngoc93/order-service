package com.esoft.order.api.mapers;

import com.esoft.order.api.requests.CreateOrderRequest;
import com.esoft.order.api.requests.UpdateOrderRequest;
import com.esoft.order.core.entities.OrderEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public abstract class OrderRequestMapper {

    public abstract OrderEntity mapCreateOrderRequestToOrderEntity(CreateOrderRequest request);
    public abstract OrderEntity mapUpdateOrderRequestToOrderEntity(UpdateOrderRequest request);
}
