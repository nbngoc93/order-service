package com.esoft.order.api.requests;

import com.esoft.order.core.enums.OrderCategory;
import com.esoft.order.core.enums.ServiceName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class CreateOrderRequest {

    @NotNull
    private OrderCategory category;

    @NotNull
    private ServiceName serviceName;

    private String description;

    private String notes;

    @NotNull
    private Integer itemsQuantity;

    @NotNull
    private BigDecimal totalAmount;
}
