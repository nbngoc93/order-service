package com.esoft.order.api.controllers;

import com.esoft.order.adapter.services.OrderService;
import com.esoft.order.api.mapers.OrderRequestMapper;
import com.esoft.order.api.mapers.OrderResponseMapper;
import com.esoft.order.api.requests.CreateOrderRequest;
import com.esoft.order.api.requests.UpdateOrderRequest;
import com.esoft.order.api.resources.MetaResource;
import com.esoft.order.api.resources.Resource;
import com.esoft.order.api.responses.OrderResponse;
import com.esoft.order.api.responses.OrderSummaryResponse;
import com.esoft.order.core.entities.OrderEntity;
import com.esoft.order.core.entities.OrderSummaryEntity;
import com.esoft.order.core.entities.PageEntity;
import com.esoft.order.core.filters.PageFilter;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.Instant;
import java.util.List;

@RestController
@RequiredArgsConstructor
@Validated
public class OrderController {

    private final OrderRequestMapper orderRequestMapper;
    private final OrderResponseMapper orderResponseMapper;

    private final OrderService orderService;

    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @GetMapping("/v1/orders")
    public Resource<List<OrderResponse>> listOrders(
            @Parameter(hidden = true) @AuthenticationPrincipal UserDetails userDetail,
            @RequestParam(name = "user_id", required = false) String userIdFilter,
            @RequestParam(name = "size", required = false, defaultValue = "10") Integer pageSize,
            @RequestParam(name = "page", required = false, defaultValue = "1") Integer pageNumber
    ) {
        PageFilter pageFilter = PageFilter.of(pageSize, pageNumber);

        if (userDetail.getAuthorities().contains(new SimpleGrantedAuthority("CUSTOMER"))
                && !userDetail.getAuthorities().contains(new SimpleGrantedAuthority("ADMIN"))) {
            userIdFilter = userDetail.getUsername();
        }
        PageEntity<OrderEntity> entitiesPage = orderService.getListOrder(userIdFilter, pageFilter);

        MetaResource.PaginationResource pagination = new MetaResource.PaginationResource(entitiesPage.getCurrentPage(),
                entitiesPage.getPageSize(), entitiesPage.getTotalItems(), entitiesPage.getTotalPages());

        return new Resource<>(orderResponseMapper.mapOrderEntitiesToOrderResponses(entitiesPage.getData()), pagination);
    }

    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @PostMapping("/v1/orders")
    public Resource<OrderResponse> createOrder(
            @Parameter(hidden = true) @AuthenticationPrincipal UserDetails userDetail,
            @Valid @RequestBody CreateOrderRequest request
    ) {
        OrderEntity orderEntity = orderRequestMapper.mapCreateOrderRequestToOrderEntity(request);
        orderEntity.setUserId(userDetail.getUsername());

        OrderEntity entity = orderService.createOrder(orderEntity);

        return new Resource<>(orderResponseMapper.mapOrderEntityToOrderResponse(entity));
    }

    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @GetMapping("/v1/orders/{orderReference}")
    public Resource<OrderResponse> getOrder(
            @PathVariable("orderReference") String orderReference
    ) {
        OrderEntity orderEntity = orderService.getOrder(orderReference);

        return new Resource<>(orderResponseMapper.mapOrderEntityToOrderResponse(orderEntity));
    }

    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @PutMapping("/v1/orders/{orderReference}")
    public Resource<OrderResponse> updateOrder(
            @PathVariable("orderReference") String orderReference,
            @RequestBody UpdateOrderRequest request
    ) {
        OrderEntity orderUpdate = orderRequestMapper.mapUpdateOrderRequestToOrderEntity(request);
        OrderEntity orderEntity = orderService.updateOrder(orderReference, orderUpdate);

        return new Resource<>(orderResponseMapper.mapOrderEntityToOrderResponse(orderEntity));
    }

    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @DeleteMapping("/v1/orders/{orderReference}")
    public Resource<Object> deleteOrder(
            @PathVariable("orderReference") String orderReference
    ) {
        orderService.deleteOrder(orderReference);

        return new Resource<>(null);
    }

    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @GetMapping("/v1/orders/summary")
    public Resource<OrderSummaryResponse> getSummary(
            @Parameter(hidden = true) @AuthenticationPrincipal UserDetails userDetail,
            @RequestParam(name = "user_id", required = false) String userIdFilter,
            @RequestParam(name = "time_from", required = false) Instant timeFrom,
            @RequestParam(name = "time_to", required = false) Instant timeTo
    ) {
        if (userDetail.getAuthorities().contains(new SimpleGrantedAuthority("CUSTOMER"))
                && !userDetail.getAuthorities().contains(new SimpleGrantedAuthority("ADMIN"))) {
            userIdFilter = userDetail.getUsername();
        }
        OrderSummaryEntity summary = orderService.getSummary(userIdFilter, timeFrom, timeTo);
        return new Resource<>(orderResponseMapper.mapOrderSummaryEntityToResponse(summary));
    }
}
