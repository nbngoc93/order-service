package com.esoft.order.api.test;


import com.esoft.order.Application;
import com.esoft.order.api.utils.TokenUtil;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;

import java.util.HashMap;

@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = {"test"})
public class BaseTest {

    @LocalServerPort
    protected int serverPort;

    @Autowired
    private TokenUtil tokenUtil;

    @BeforeEach
    public final void setup() {
        RestAssured.port = this.serverPort;

        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setContentType(MediaType.APPLICATION_JSON_VALUE)
                .setAccept(MediaType.APPLICATION_JSON_VALUE)
                .build();
    }

    public String generateJwt(String userId, String role) {
        return "Bearer " + tokenUtil.createDefaultJwt(userId, new HashMap<>(){{put("role", role);}});
    }
}
