package com.esoft.order.api.utils;

import com.esoft.order.adapter.mysql.models.OrderModel;
import com.esoft.order.core.enums.OrderCategory;
import com.esoft.order.core.enums.ServiceName;

import java.math.BigDecimal;
import java.util.UUID;

public class ModelBuilderUtil {

    public static OrderModel buildOrderModel() {
        return OrderModel.builder()
                .userId("1")
                .reference(UUID.randomUUID().toString().replace("-", ""))
                .category(OrderCategory.LUXURY)
                .serviceName(ServiceName.PHOTO_EDITING)
                .description("Some description")
                .notes("Some note")
                .itemsQuantity(11)
                .totalAmount(BigDecimal.valueOf(10000))
                .build();
    }

    public static OrderModel buildOrderModel(String uid) {
        return OrderModel.builder()
                .userId(uid)
                .reference(UUID.randomUUID().toString().replace("-", ""))
                .category(OrderCategory.LUXURY)
                .serviceName(ServiceName.PHOTO_EDITING)
                .description("Some description")
                .notes("Some note")
                .itemsQuantity(11)
                .totalAmount(BigDecimal.valueOf(10000))
                .build();
    }
}
