package com.esoft.order.api.test;

import com.esoft.order.adapter.mysql.models.OrderModel;
import com.esoft.order.adapter.mysql.repositories.OrderRepository;
import com.esoft.order.api.utils.ModelBuilderUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;


import static org.junit.jupiter.api.Assertions.*;

public class OrderRepoTest extends BaseTest {

    @Autowired
    private OrderRepository orderRepository;

    @Test
    public void testOrderRepositorySaveModel_success() {
        OrderModel orderModel = ModelBuilderUtil.buildOrderModel();

        orderRepository.save(orderModel);
        List<OrderModel> orderModels = orderRepository.findAll();

        assertEquals(orderModels.size(), 1);

        OrderModel orderModelSaved = orderModels.get(0);

        assertEquals(orderModelSaved.getItemsQuantity(), orderModel.getItemsQuantity());
        assertEquals(orderModelSaved.getItemsQuantity(), orderModel.getItemsQuantity());
        assertEquals(orderModelSaved.getReference(), orderModel.getReference());
        assertEquals(orderModelSaved.getDescription(), orderModel.getDescription());
    }

    @AfterEach
    public void clean() {
        orderRepository.deleteAll();
    }
}
