package com.esoft.order.api.test.controllers;

import com.esoft.order.adapter.mysql.models.OrderModel;
import com.esoft.order.adapter.mysql.repositories.OrderRepository;
import com.esoft.order.api.requests.CreateOrderRequest;
import com.esoft.order.api.requests.UpdateOrderRequest;
import com.esoft.order.api.test.BaseTest;
import com.esoft.order.api.utils.ModelBuilderUtil;
import com.esoft.order.core.enums.OrderCategory;
import com.esoft.order.core.enums.ServiceName;
import com.esoft.order.core.exceptions.Error;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import lombok.SneakyThrows;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;

import java.math.BigDecimal;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.*;

public class OrderControllerTest extends BaseTest {
    private static final String LIST_ORDER_PATH = "/v1/orders";
    private static final String CREATE_ORDER_PATH = "/v1/orders";
    private static final String GET_ORDER_PATH = "/v1/orders/";
    private static final String GET_ORDER_SUMMARY_PATH = "/v1/orders/summary";

    @Autowired
    private OrderRepository orderRepository;

    private final ObjectMapper objectMapper =  new ObjectMapper()
            .setPropertyNamingStrategy(PropertyNamingStrategies.SNAKE_CASE);

    @Test
    @SneakyThrows
    public void testUnAuthorize_thenReturn401() {
        given()
                .when()
                .post(LIST_ORDER_PATH)
                .then()
                .statusCode(HttpStatus.SC_UNAUTHORIZED);
    }

    @Test
    @SneakyThrows
    public void testWrongToken_thenReturn401() {
        given()
                .when()
                .header(HttpHeaders.AUTHORIZATION, "WRONG_TOKEN")
                .post(LIST_ORDER_PATH)
                .then()
                .statusCode(HttpStatus.SC_UNAUTHORIZED);
    }

    @Test
    @SneakyThrows
    public void testCreateOrder_thenReturnSuccess() {
        CreateOrderRequest createOrderRequest = CreateOrderRequest.builder()
                .category(OrderCategory.SUPREME_LUXURY)
                .serviceName(ServiceName.VIDEO_EDITING)
                .description("Some description")
                .notes("Some note")
                .itemsQuantity(99)
                .totalAmount(BigDecimal.valueOf(1000000))
                .build();

        given()
                .when()
                .header(HttpHeaders.AUTHORIZATION, generateJwt("99", "CUSTOMER"))
                .body(objectMapper.writeValueAsString(createOrderRequest))
                .post(CREATE_ORDER_PATH)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("data.user_id", equalTo("99"))
                .body("data.reference", notNullValue())
                .body("data.category", equalTo(createOrderRequest.getCategory().name().toUpperCase()))
                .body("data.service_name", equalTo(createOrderRequest.getServiceName().name().toUpperCase()))
                .body("data.items_quantity", equalTo(createOrderRequest.getItemsQuantity()))
                .body("data.total_amount", equalTo(createOrderRequest.getTotalAmount().intValue()));
    }

    @Test
    @SneakyThrows
    public void testCreateOrder_whenMissingOrderCategory_thenReturn400() {
        CreateOrderRequest createOrderRequest = CreateOrderRequest.builder()
                .serviceName(ServiceName.VIDEO_EDITING)
                .description("Some description")
                .notes("Some note")
                .itemsQuantity(99)
                .totalAmount(BigDecimal.valueOf(1000000))
                .build();

        given()
                .when()
                .header(HttpHeaders.AUTHORIZATION, generateJwt("99", "CUSTOMER"))
                .body(objectMapper.writeValueAsString(createOrderRequest))
                .post(CREATE_ORDER_PATH)
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("meta.code", equalTo(400));
    }

    @Test
    @SneakyThrows
    public void testGetOrder_thenSuccess() {
        OrderModel orderModel = orderRepository.save(ModelBuilderUtil.buildOrderModel());

        given()
                .when()
                .header(HttpHeaders.AUTHORIZATION, generateJwt(orderModel.getUserId(), "CUSTOMER"))
                .get(GET_ORDER_PATH + orderModel.getReference())
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("meta.code", equalTo(200))
                .body("data.reference", notNullValue())
                .body("data.category", equalTo(orderModel.getCategory().name().toUpperCase()))
                .body("data.service_name", equalTo(orderModel.getServiceName().name().toUpperCase()))
                .body("data.items_quantity", equalTo(orderModel.getItemsQuantity()))
                .body("data.total_amount", equalTo(orderModel.getTotalAmount().floatValue()));
    }

    @Test
    @SneakyThrows
    public void testGetOrder_whenUserDoesNotOwnTheOrder_thenReturnNotFound() {
        OrderModel orderModel = orderRepository.save(ModelBuilderUtil.buildOrderModel());

        given()
                .when()
                .header(HttpHeaders.AUTHORIZATION, generateJwt("99", "CUSTOMER"))
                .get(GET_ORDER_PATH + orderModel.getReference())
                .then()
                .statusCode(HttpStatus.SC_FORBIDDEN)
                .body("meta.code", equalTo(Error.FORBIDDEN.getCode()));
    }

    @Test
    @SneakyThrows
    public void testGetOrder_whenUserIsAdminNotCreateTheOrder_thenReturnSuccess() {
        OrderModel orderModel = orderRepository.save(ModelBuilderUtil.buildOrderModel());

        given()
                .when()
                .header(HttpHeaders.AUTHORIZATION, generateJwt("99", "ADMIN"))
                .get(GET_ORDER_PATH + orderModel.getReference())
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("meta.code", equalTo(200))
                .body("data.user_id", equalTo(orderModel.getUserId()))
                .body("data.reference", notNullValue())
                .body("data.category", equalTo(orderModel.getCategory().name().toUpperCase()))
                .body("data.service_name", equalTo(orderModel.getServiceName().name().toUpperCase()))
                .body("data.items_quantity", equalTo(orderModel.getItemsQuantity()))
                .body("data.total_amount", equalTo(orderModel.getTotalAmount().floatValue()));
    }

    @Test
    @SneakyThrows
    public void testUpdateOrder_thenReturnSuccess() {
        OrderModel orderModel = orderRepository.save(ModelBuilderUtil.buildOrderModel());

        UpdateOrderRequest updateOrderRequest = UpdateOrderRequest.builder()
                .category(OrderCategory.SUPREME_LUXURY)
                .serviceName(ServiceName.VIDEO_EDITING)
                .description("note updated")
                .notes("note updated")
                .itemsQuantity(5)
                .build();

        given()
                .when()
                .header(HttpHeaders.AUTHORIZATION, generateJwt(orderModel.getUserId(), "CUSTOMER"))
                .body(objectMapper.writeValueAsString(updateOrderRequest))
                .put(GET_ORDER_PATH + orderModel.getReference())
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("meta.code", equalTo(200))
                .body("data.user_id", equalTo(orderModel.getUserId()))
                .body("data.reference", equalTo(orderModel.getReference()))
                .body("data.category", equalTo(updateOrderRequest.getCategory().name().toUpperCase()))
                .body("data.service_name", equalTo(updateOrderRequest.getServiceName().name().toUpperCase()))
                .body("data.description", equalTo(updateOrderRequest.getDescription()))
                .body("data.notes", equalTo(updateOrderRequest.getNotes()))
                .body("data.items_quantity", equalTo(updateOrderRequest.getItemsQuantity()))
                .body("data.total_amount", equalTo(orderModel.getTotalAmount().floatValue()));
    }

    @Test
    @SneakyThrows
    public void testDeleteOrder_thenReturnSuccess() {
        OrderModel orderModel = orderRepository.save(ModelBuilderUtil.buildOrderModel());

        given()
                .when()
                .header(HttpHeaders.AUTHORIZATION, generateJwt(orderModel.getUserId(), "CUSTOMER"))
                .delete(GET_ORDER_PATH + orderModel.getReference())
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("meta.code", equalTo(200));

        orderModel = orderRepository.findById(orderModel.getId()).get();

        assertNotNull(orderModel.getDeletedAt());

        given()
                .when()
                .header(HttpHeaders.AUTHORIZATION, generateJwt(orderModel.getUserId(), "CUSTOMER"))
                .get(GET_ORDER_PATH + orderModel.getReference())
                .then()
                .statusCode(HttpStatus.SC_NOT_FOUND)
                .body("meta.code", equalTo(Error.ORDER_NOT_FOUND.getCode()));
    }


    @Test
    @SneakyThrows
    public void testGetOrderSummary_thenReturnSuccess() {
        OrderModel orderModel = orderRepository.save(ModelBuilderUtil.buildOrderModel());
        orderRepository.save(ModelBuilderUtil.buildOrderModel());
        orderRepository.save(ModelBuilderUtil.buildOrderModel());
        orderRepository.save(ModelBuilderUtil.buildOrderModel());
        orderRepository.save(ModelBuilderUtil.buildOrderModel());
        orderRepository.save(ModelBuilderUtil.buildOrderModel());

        orderRepository.save(ModelBuilderUtil.buildOrderModel("55"));
        orderRepository.save(ModelBuilderUtil.buildOrderModel("55"));
        orderRepository.save(ModelBuilderUtil.buildOrderModel("55"));


        given()
                .when()
                .header(HttpHeaders.AUTHORIZATION, generateJwt(orderModel.getUserId(), "CUSTOMER"))
                .param("user_id", orderModel.getUserId())
                .get(GET_ORDER_SUMMARY_PATH)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("meta.code", equalTo(200))
                .body("data.number_of_orders", equalTo(6))
                .body("data.revenue.intValue()", equalTo(60000));
    }

    @Test
    @SneakyThrows
    public void testGetOrderSummary_withUserAdmin_thenCanViewCustomerOrderReturnSuccess() {

        OrderModel orderModel = orderRepository.save(ModelBuilderUtil.buildOrderModel());
        orderRepository.save(ModelBuilderUtil.buildOrderModel());
        orderRepository.save(ModelBuilderUtil.buildOrderModel());
        orderRepository.save(ModelBuilderUtil.buildOrderModel());
        orderRepository.save(ModelBuilderUtil.buildOrderModel());
        orderRepository.save(ModelBuilderUtil.buildOrderModel());

        orderRepository.save(ModelBuilderUtil.buildOrderModel("55"));
        orderRepository.save(ModelBuilderUtil.buildOrderModel("55"));
        orderRepository.save(ModelBuilderUtil.buildOrderModel("55"));

        given()
                .when()
                .header(HttpHeaders.AUTHORIZATION, generateJwt(orderModel.getUserId(), "ADMIN"))
                .get(GET_ORDER_SUMMARY_PATH)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("meta.code", equalTo(200))
                .body("data.number_of_orders", equalTo(9))
                .body("data.revenue.intValue()", equalTo(90000));
    }

    @Test
    @SneakyThrows
    public void testGetListOrder_withDefaultPageFilter_thenReturnSuccess() {
        OrderModel orderModel = orderRepository.save(ModelBuilderUtil.buildOrderModel());
        orderRepository.save(ModelBuilderUtil.buildOrderModel());
        orderRepository.save(ModelBuilderUtil.buildOrderModel());
        orderRepository.save(ModelBuilderUtil.buildOrderModel());
        orderRepository.save(ModelBuilderUtil.buildOrderModel());
        orderRepository.save(ModelBuilderUtil.buildOrderModel());

        orderRepository.save(ModelBuilderUtil.buildOrderModel("55"));
        orderRepository.save(ModelBuilderUtil.buildOrderModel("55"));
        orderRepository.save(ModelBuilderUtil.buildOrderModel("55"));

        given()
                .when()
                .header(HttpHeaders.AUTHORIZATION, generateJwt(orderModel.getUserId(), "CUSTOMER"))
                .get(LIST_ORDER_PATH)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("meta.code", equalTo(200))
                .body("meta.page", notNullValue())
                .body("meta.page.current_page", equalTo(1))
                .body("meta.page.page_size", equalTo(10))
                .body("meta.page.total_items", equalTo(6))
                .body("meta.page.total_pages", equalTo(1))
                .body("data", hasSize(6));
    }

    @Test
    @SneakyThrows
    public void testGetListOrder_withAmin_thenReturnSuccess() {
        OrderModel orderModel = orderRepository.save(ModelBuilderUtil.buildOrderModel());
        orderRepository.save(ModelBuilderUtil.buildOrderModel());
        orderRepository.save(ModelBuilderUtil.buildOrderModel());
        orderRepository.save(ModelBuilderUtil.buildOrderModel());
        orderRepository.save(ModelBuilderUtil.buildOrderModel());
        orderRepository.save(ModelBuilderUtil.buildOrderModel());

        orderRepository.save(ModelBuilderUtil.buildOrderModel("55"));
        orderRepository.save(ModelBuilderUtil.buildOrderModel("55"));
        orderRepository.save(ModelBuilderUtil.buildOrderModel("55"));

        given()
                .when()
                .header(HttpHeaders.AUTHORIZATION, generateJwt(orderModel.getUserId(), "ADMIN"))
                .get(LIST_ORDER_PATH)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("meta.code", equalTo(200))
                .body("meta.page", notNullValue())
                .body("meta.page.current_page", equalTo(1))
                .body("meta.page.page_size", equalTo(10))
                .body("meta.page.total_items", equalTo(9))
                .body("meta.page.total_pages", equalTo(1))
                .body("data", hasSize(9));
    }

    @AfterEach
    public void clean() {
        orderRepository.deleteAll();
    }
}
