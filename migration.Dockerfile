FROM openjdk:11.0.16-jre-slim

VOLUME /tmp

ENV TZ="Asia/Ho_Chi_Minh"

RUN apt-get update \
    && apt-get install -y unzip curl

RUN mkdir -p /app/lib && mkdir -p /app/META-INF && mkdir -p /target/dependency && mkdir /build
COPY esoft-order-service-migration/target/esoft-order-service-migration-*.jar /build/esoft-order-service-migration.jar
RUN (cd /target/dependency; unzip ../../build/esoft-order-service-migration.jar)
RUN cp -r /target/dependency/BOOT-INF/lib/* /app/lib
RUN cp -r /target/dependency/META-INF/* /app/META-INF
RUN cp -r /target/dependency/BOOT-INF/classes/* /app
RUN rm -rf /target/dependency
RUN rm -rf /build

CMD ["/bin/bash", "-c", "java $JAVA_OPT -cp app:app/lib/* com.esoft.order.Migration"]
