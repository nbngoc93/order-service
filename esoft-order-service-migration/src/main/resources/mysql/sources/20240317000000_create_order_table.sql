CREATE TABLE IF NOT EXISTS `orders` (
    `id`                        INT             NOT NULL AUTO_INCREMENT,
    `created_at`                DATETIME        NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at`                DATETIME        NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `deleted_at`                DATETIME        DEFAULT NULL,
    `user_id`                   VARCHAR(255)    NOT NULL,
    `reference`                 VARCHAR(255)    NOT NULL UNIQUE,
    `category`                  VARCHAR(255)    NOT NULL,
    `service_name`              VARCHAR(255)    NOT NULL,
    `description`               TEXT,
    `notes`                     TEXT,
    `items_quantity`            INT             NOT NULL,
    `total_amount`              DECIMAL(18, 3)  NOT NULL DEFAULT 0,
    INDEX `user_id index` (`user_id`),
    INDEX `order_reference index` (`reference`),
    PRIMARY KEY (`id`)
)