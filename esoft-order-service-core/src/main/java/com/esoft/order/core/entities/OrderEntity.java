package com.esoft.order.core.entities;

import com.esoft.order.core.enums.OrderCategory;
import com.esoft.order.core.enums.ServiceName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.math.BigDecimal;
import java.time.Instant;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class OrderEntity {

    private Integer id;
    private Instant createdAt;
    private Instant updatedAt;
    private Instant deletedAt;
    private String userId;
    private String reference;
    private OrderCategory category;
    private ServiceName serviceName;
    private String description;
    private String notes;
    private Integer itemsQuantity;
    private BigDecimal totalAmount;
}
