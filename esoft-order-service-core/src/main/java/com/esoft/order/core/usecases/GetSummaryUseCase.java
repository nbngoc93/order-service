package com.esoft.order.core.usecases;

import com.esoft.order.core.entities.OrderSummaryEntity;
import com.esoft.order.core.ports.OrderSummaryRepositoryPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
@RequiredArgsConstructor
public class GetSummaryUseCase {

    private final OrderSummaryRepositoryPort orderSummaryRepository;

    public OrderSummaryEntity getSummary(String userIdFilter, Instant fromTime, Instant toTime) {
        return orderSummaryRepository.getSummary(userIdFilter, fromTime, toTime);
    }
}
