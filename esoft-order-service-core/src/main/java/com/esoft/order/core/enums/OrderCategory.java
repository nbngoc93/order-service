package com.esoft.order.core.enums;

public enum OrderCategory {
    LUXURY,
    SUPER_LUXURY,
    SUPREME_LUXURY
    ;
}
