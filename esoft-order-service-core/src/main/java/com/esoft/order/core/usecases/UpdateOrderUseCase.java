package com.esoft.order.core.usecases;

import com.esoft.order.core.entities.OrderEntity;
import com.esoft.order.core.ports.OrderRepositoryPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
@RequiredArgsConstructor
public class UpdateOrderUseCase {

    private final OrderRepositoryPort orderRepositoryPort;
    private final GetOrderUseCase getOrderUseCase;

    public OrderEntity updateOrder(String orderRef, OrderEntity orderUpdateData) {
        OrderEntity orderEntity = getOrderUseCase.getOrder(orderRef);

        if (orderUpdateData.getCategory() != null) {
            orderEntity.setCategory(orderUpdateData.getCategory());
        }

        if (orderUpdateData.getServiceName() != null) {
            orderEntity.setServiceName(orderUpdateData.getServiceName());
        }

        if (StringUtils.hasLength(orderUpdateData.getDescription())) {
            orderEntity.setDescription(orderUpdateData.getDescription());
        }

        if (StringUtils.hasLength(orderUpdateData.getNotes())) {
            orderEntity.setNotes(orderUpdateData.getNotes());
        }

        if (orderUpdateData.getItemsQuantity() != null) {
            orderEntity.setItemsQuantity(orderUpdateData.getItemsQuantity());
        }

        if (orderUpdateData.getTotalAmount() != null) {
            orderEntity.setTotalAmount(orderUpdateData.getTotalAmount());
        }

        return orderRepositoryPort.save(orderEntity);
    }
}
