package com.esoft.order.core.ports;

import com.esoft.order.core.entities.OrderEntity;

public interface PermissionServicePort {
    void checkPermission(OrderEntity orderEntity);
}
