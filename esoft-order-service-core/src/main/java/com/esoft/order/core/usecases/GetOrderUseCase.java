package com.esoft.order.core.usecases;


import com.esoft.order.core.entities.OrderEntity;
import com.esoft.order.core.exceptions.ApplicationException;
import com.esoft.order.core.exceptions.Error;
import com.esoft.order.core.ports.OrderRepositoryPort;
import com.esoft.order.core.ports.PermissionServicePort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GetOrderUseCase {

    private final OrderRepositoryPort orderRepositoryPort;
    private final PermissionServicePort permissionServicePort;

    public OrderEntity getOrder(String orderRef) {
        OrderEntity orderEntity = orderRepositoryPort.findByReferenceAndDeletedAtIsNull(orderRef)
                .orElseThrow(() -> new ApplicationException(Error.ORDER_NOT_FOUND));
        permissionServicePort.checkPermission(orderEntity);
        return orderEntity;
    }
}
