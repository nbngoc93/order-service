package com.esoft.order.core.usecases;


import com.esoft.order.core.entities.OrderEntity;
import com.esoft.order.core.ports.OrderRepositoryPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class CreateOrderUseCase {

    private final OrderRepositoryPort orderRepositoryPort;

    public OrderEntity createOrder(OrderEntity orderEntity) {
        orderEntity.setReference(UUID.randomUUID().toString().replace("-", ""));
        return orderRepositoryPort.save(orderEntity);
    }
}
