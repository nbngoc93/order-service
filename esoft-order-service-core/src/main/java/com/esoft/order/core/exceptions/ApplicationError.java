package com.esoft.order.core.exceptions;

public interface ApplicationError {

    Integer getCode();
    Integer getHttpCode();
    String getMessage();
}
