package com.esoft.order.core.ports;

import com.esoft.order.core.entities.OrderSummaryEntity;

import java.time.Instant;

public interface OrderSummaryRepositoryPort {
    OrderSummaryEntity getSummary(String userId, Instant fromTime, Instant toTime);
}
