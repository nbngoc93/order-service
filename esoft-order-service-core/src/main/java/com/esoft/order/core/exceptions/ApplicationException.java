package com.esoft.order.core.exceptions;

import lombok.Getter;

@Getter
public class ApplicationException extends RuntimeException {
    private final int code;
    private final Integer httpCode;

    public ApplicationException(ApplicationError applicationError) {
        super(applicationError.getMessage());
        this.code = applicationError.getCode();
        this.httpCode = applicationError.getHttpCode();
    }
}
