package com.esoft.order.core.usecases;

import com.esoft.order.core.entities.OrderEntity;
import com.esoft.order.core.entities.PageEntity;
import com.esoft.order.core.filters.PageFilter;
import com.esoft.order.core.ports.OrderRepositoryPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ListOrderUseCase {

    private final OrderRepositoryPort orderRepositoryPort;

    public PageEntity<OrderEntity> getListOrder(String userIdFilter, PageFilter pageFilter) {
        return orderRepositoryPort
                .findAllByUserIdAndDeletedAtIsNull(userIdFilter, pageFilter.getPageSize(),pageFilter.getPageNumber());
    }
}
