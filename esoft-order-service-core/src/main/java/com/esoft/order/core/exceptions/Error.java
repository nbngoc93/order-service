package com.esoft.order.core.exceptions;

public enum Error implements ApplicationError {

    UNAUTHORIZED(40100000, "unauthorized"),
    FORBIDDEN(40300000, "Forbidden"),
    DEFAULT_NOT_FOUND(40400000, "Not found resource"),
    ORDER_NOT_FOUND(40400001, "Order not found")

    ;
    private final int code;
    private final String message;

    Error(int code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public Integer getHttpCode() {
        return code/100000;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
