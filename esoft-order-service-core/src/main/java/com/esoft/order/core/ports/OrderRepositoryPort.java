package com.esoft.order.core.ports;

import com.esoft.order.core.entities.OrderEntity;
import com.esoft.order.core.entities.PageEntity;

import java.util.Optional;

public interface OrderRepositoryPort {
    OrderEntity save(OrderEntity orderEntity);
    Optional<OrderEntity> findByReferenceAndUserIdAndDeletedAtIsNull(String orderRef, Integer userId);
    Optional<OrderEntity> findByReferenceAndDeletedAtIsNull(String orderRef);

    PageEntity<OrderEntity> findAllByUserIdAndDeletedAtIsNull(String userId, Integer pageSize, Integer pageNumber);
}
