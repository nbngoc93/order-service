package com.esoft.order.core.usecases;

import com.esoft.order.core.entities.OrderEntity;
import com.esoft.order.core.ports.OrderRepositoryPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
@RequiredArgsConstructor
public class DeleteOrderUseCase {

    private final OrderRepositoryPort orderRepositoryPort;
    private final GetOrderUseCase getOrderUseCase;

    public void deleteOrder(String orderRef) {
        OrderEntity orderEntity = getOrderUseCase.getOrder(orderRef);

        orderEntity.setDeletedAt(Instant.now());

        orderRepositoryPort.save(orderEntity);
    }
}
