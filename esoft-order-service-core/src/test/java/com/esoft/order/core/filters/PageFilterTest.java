package com.esoft.order.core.filters;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PageFilterTest {

    @Test
    public void testBuildPageFilter() {
        PageFilter pageFilter = PageFilter.of(-1, 0);
        assertEquals(pageFilter.getPageSize(), PageFilter.DEFAULT_PAGE_SIZE);
        assertEquals(pageFilter.getPageNumber(), PageFilter.DEFAULT_FIRST_PAGE);
    }
}