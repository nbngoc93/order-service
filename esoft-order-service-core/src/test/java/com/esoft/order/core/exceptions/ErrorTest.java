package com.esoft.order.core.exceptions;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ErrorTest {
    @Test
    public void testGetHttpCode() {
        assertEquals(Error.DEFAULT_NOT_FOUND.getHttpCode(), 404);
    }
}