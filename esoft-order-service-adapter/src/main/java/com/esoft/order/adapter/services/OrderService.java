package com.esoft.order.adapter.services;

import com.esoft.order.core.entities.OrderEntity;
import com.esoft.order.core.entities.OrderSummaryEntity;
import com.esoft.order.core.entities.PageEntity;
import com.esoft.order.core.filters.PageFilter;
import com.esoft.order.core.usecases.CreateOrderUseCase;
import com.esoft.order.core.usecases.DeleteOrderUseCase;
import com.esoft.order.core.usecases.GetOrderUseCase;
import com.esoft.order.core.usecases.GetSummaryUseCase;
import com.esoft.order.core.usecases.ListOrderUseCase;
import com.esoft.order.core.usecases.UpdateOrderUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
@RequiredArgsConstructor
public class OrderService {

    private final CreateOrderUseCase createOrderUseCase;
    private final GetOrderUseCase getOrderUseCase;
    private final UpdateOrderUseCase updateOrderUseCase;
    private final DeleteOrderUseCase deleteOrderUseCase;
    private final GetSummaryUseCase getSummaryUseCase;
    private final ListOrderUseCase listOrderUseCase;


    public PageEntity<OrderEntity> getListOrder(String userId, PageFilter pageFilter) {
        return listOrderUseCase.getListOrder(userId, pageFilter);
    }

    public OrderEntity createOrder(OrderEntity orderEntity) {
        return createOrderUseCase.createOrder(orderEntity);
    }

    public OrderEntity getOrder(String orderRef) {
        return getOrderUseCase.getOrder(orderRef);
    }

    public OrderEntity updateOrder(String orderRef, OrderEntity orderUpdateData) {
        return updateOrderUseCase.updateOrder(orderRef, orderUpdateData);
    }

    public void deleteOrder(String orderRef) {
        deleteOrderUseCase.deleteOrder(orderRef);
    }

    public OrderSummaryEntity getSummary(String userIdFilter, Instant fromTime, Instant toTime) {
        return getSummaryUseCase.getSummary(userIdFilter, fromTime, toTime);
    }
}
