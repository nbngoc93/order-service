package com.esoft.order.adapter.adapters;

import com.esoft.order.adapter.mappers.OrderModelMapper;
import com.esoft.order.adapter.mysql.models.OrderModel;
import com.esoft.order.adapter.mysql.repositories.OrderRepository;
import com.esoft.order.core.entities.OrderEntity;
import com.esoft.order.core.entities.PageEntity;
import com.esoft.order.core.ports.OrderRepositoryPort;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class OrderRepositoryAdapter implements OrderRepositoryPort {

    private final OrderRepository orderRepository;

    private final OrderModelMapper orderModelMapper;

    @Override
    public OrderEntity save(OrderEntity orderEntity) {
        return orderModelMapper.fromModelToEntity(orderRepository.save(orderModelMapper.fromEntityToModel(orderEntity)));
    }

    @Override
    public Optional<OrderEntity> findByReferenceAndUserIdAndDeletedAtIsNull(String orderRef, Integer userId) {
        return orderRepository.findByReferenceAndUserIdAndDeletedAtIsNull(orderRef, userId)
                .map(orderModelMapper::fromModelToEntity);
    }

    @Override
    public Optional<OrderEntity> findByReferenceAndDeletedAtIsNull(String orderRef) {
        return orderRepository.findByReferenceAndDeletedAtIsNull(orderRef)
                .map(orderModelMapper::fromModelToEntity);
    }

    @Override
    public PageEntity<OrderEntity> findAllByUserIdAndDeletedAtIsNull(String userId, Integer pageSize, Integer pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber - 1, pageSize);

        Specification<OrderModel> orderModelSpecification = (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(criteriaBuilder.isNull(root.get("deletedAt")));
            if (userId != null) {
                predicates.add(criteriaBuilder.equal(root.get("userId"), userId));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };

        Page<OrderModel> orderModelsPage = orderRepository.findAll(orderModelSpecification, pageable);

        return new PageEntity<>(orderModelMapper.fromModelsToEntities(orderModelsPage.getContent()),
                pageNumber, pageSize,
                orderModelsPage.getTotalElements(), orderModelsPage.getTotalPages());
    }
}
