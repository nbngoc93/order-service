package com.esoft.order.adapter.mysql.repositories;

import com.esoft.order.adapter.mysql.models.OrderModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<OrderModel, Long>, JpaSpecificationExecutor<OrderModel> {

    Optional<OrderModel> findByReferenceAndUserIdAndDeletedAtIsNull(String orderRef, Integer userId);
    Optional<OrderModel> findByReferenceAndDeletedAtIsNull(String orderRef);
}
