package com.esoft.order.adapter.mappers;

import com.esoft.order.adapter.mysql.models.OrderModel;
import com.esoft.order.core.entities.OrderEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class OrderModelMapper {

    public abstract OrderEntity fromModelToEntity(OrderModel model);
    public abstract List<OrderEntity> fromModelsToEntities(List<OrderModel> models);
    public abstract OrderModel fromEntityToModel(OrderEntity entity);
}
