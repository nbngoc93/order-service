package com.esoft.order.adapter.adapters;

import com.esoft.order.core.entities.OrderSummaryEntity;
import com.esoft.order.core.ports.OrderSummaryRepositoryPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.math.BigDecimal;
import java.time.Instant;

@Service
@RequiredArgsConstructor
public class SummaryRepositoryAdapter implements OrderSummaryRepositoryPort {

    private final EntityManager entityManager;

    @Override
    public OrderSummaryEntity getSummary(String userId, Instant fromTime, Instant toTime) {
        String orderSummaryEntityClassName = OrderSummaryEntity.class.getName();
        StringBuilder queryStringBuilder = new StringBuilder("SELECT NEW ");
        queryStringBuilder.append(orderSummaryEntityClassName);
        queryStringBuilder.append("(COUNT(*), SUM(o.totalAmount)) FROM OrderModel o WHERE o.deletedAt IS NULL ");

        if (StringUtils.hasLength(userId)) {
            queryStringBuilder.append("AND o.userId = :userId ");
        }
        if (fromTime != null) {
            queryStringBuilder.append("AND o.createdAt >= :fromTime ");
        }
        if (toTime != null) {
            queryStringBuilder.append("AND o.createdAt < :toTime ");
        }

        TypedQuery<OrderSummaryEntity> query = entityManager.createQuery(
                queryStringBuilder.toString(),
                OrderSummaryEntity.class
        );

        if (userId != null) {
            query.setParameter("userId", userId);
        }
        if (fromTime != null) {
            query.setParameter("fromTime", fromTime);
        }
        if (toTime != null) {
            query.setParameter("toTime", toTime);
        }

        OrderSummaryEntity summary = query.getSingleResult();
        if (summary.getRevenue() == null) {
            summary.setRevenue(BigDecimal.ZERO);
        }
        return summary;
    }
}
