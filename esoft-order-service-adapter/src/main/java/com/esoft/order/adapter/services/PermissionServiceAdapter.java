package com.esoft.order.adapter.services;

import com.esoft.order.core.entities.OrderEntity;
import com.esoft.order.core.exceptions.ApplicationException;
import com.esoft.order.core.exceptions.Error;
import com.esoft.order.core.ports.PermissionServicePort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class PermissionServiceAdapter implements PermissionServicePort {


    @Override
    public void checkPermission(OrderEntity orderEntity) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            throw new ApplicationException(Error.UNAUTHORIZED);
        }
        if (authentication.getPrincipal() instanceof UserDetails) {
            UserDetails userDetails = (UserDetails) authentication.getPrincipal();
            if (!userDetails.getAuthorities().contains(new SimpleGrantedAuthority("ADMIN"))
            && userDetails.getAuthorities().contains(new SimpleGrantedAuthority("CUSTOMER"))
            && !userDetails.getUsername().equals(orderEntity.getUserId())) {
                throw new ApplicationException(Error.FORBIDDEN);
            }
        }
    }
}
