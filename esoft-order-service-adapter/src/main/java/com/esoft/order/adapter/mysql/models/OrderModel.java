package com.esoft.order.adapter.mysql.models;

import com.esoft.order.core.enums.OrderCategory;
import com.esoft.order.core.enums.ServiceName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Table(name = "orders")
public class OrderModel extends BaseModel {

    @Column(name = "user_id")
    private String userId;

    @Column(name = "reference")
    private String reference;

    @Column(name = "category")
    @Enumerated(EnumType.STRING)
    private OrderCategory category;

    @Column(name = "service_name")
    @Enumerated(EnumType.STRING)
    private ServiceName serviceName;

    @Column(name = "description")
    private String description;

    @Column(name = "notes")
    private String notes;

    @Column(name = "items_quantity")
    private Integer itemsQuantity;

    @Column(name = "total_amount")
    private BigDecimal totalAmount;
}
