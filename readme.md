# Order service


Run the project by run the main method in Application.java or Migration.java

The application will listen at port 8080

Use `springdoc.api-docs.enabled: true` to enable swagger and swagger ui will be available at `/swagger-ui.html`

Using Maven to build the project

### Run test
```
mvn clean verify
```

### Build
```
mvn clean package spring-boot:repackage -DskipTests
```

### Use docker to build image (Change image tag to tag of deployment):
- build api

```
docker build -t esoft-order-api:<tag> -f api.Dockerfile .
```
- build migration
```
docker build -t esoft-order-migration:<tag> -f migration.Dockerfile .
```

The docker images can use to deploy the application

### ENV variables
```yaml
app.env: specific the environment to run the application (local, dev, prod)
spring.datasource.url: url to connect db
spring.datasource.username: username to connect db
spring.datasource.password: password to connect db
app.security.jwtPublicKey: public key string to verify jwt authentication
```
example can be found in `application-local.yml` file